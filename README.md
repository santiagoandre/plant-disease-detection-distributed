# PLANT DISEASE DETECTION APPLICATION
This is a  distributed application for detect plants diseases, analyzing its leafs with cbir algorithms.

Watch video demo in this link: [Demo](https://drive.google.com/file/d/1yJ8P0OgJG7bveV--vrLl3si31QeOavG-/view?usp=sharing).

<a href="{https://drive.google.com/file/d/1yJ8P0OgJG7bveV--vrLl3si31QeOavG-/view?usp=sharing}" title="Link Title"><img src="{designs/container\ diagram.png}" alt="Demo" /></a>


# Content-Based Image Retrieval (CBIR)

The 4 Steps of Any CBIR System

No matter what Content-Based Image Retrieval System you are building, they all can be boiled down into 4 distinct steps:

1. Defining your image descriptor: At this phase, you need to decide what aspect of the image you want to describe. Are you interested in the color of the image? The shape of an object in the image? Or do you want to characterize texture?
2. Indexing your dataset: Now that you have your image descriptor defined, your job is to apply this image descriptor to each image in your dataset, extract features from these images, and write the features to storage (ex. CSV file, RDBMS, Redis, etc.) so that they can be later compared for similarity.
3. Defining your similarity metric: Cool, now you have a bunch of feature vectors. But how are you going to compare them? Popular choices include the Euclidean distance, Cosine distance, and chi-squared distance, but the actual choice is highly dependent on (1) your dataset and (2) the types of features you extracted.
4. Searching: The final step is to perform an actual search. A user will submit a query image to your system (from an upload form or via a mobile app, for instance) and your job will be to (1) extract features from this query image and then (2) apply your similarity function to compare the query features to the features already indexed. From there, you simply return the most relevant results according to your similarity function.


# Architecture and flow

## Container diagram
![ContainerDiagram](designs/container\ diagram.png)


This application is composed of Five microservices, each one complies with a specific responsibility of Content-Based Image Retrieval (CBIR) Flow. 

1. *Descriptor Microservice*: This microservice had one responsibility, describe an input image,  in other words, extract vectors, matrix of image with algorithms of DESCRIPTOR_DIC list. For this example, we use:
- Color descriptor
- Texture descriptor
- Color Tree
- SIFT
2. *Storage Microservice*: This microservice had two responsibilities, save image and vector descriptors, and query images from a features vector of input.

3. *Match Microservice*: This microservice had one responsibility, compare two features vectors of two images, and returns match percentage.
4. *Cbir Microservice*: This microservice fulfits the role of api and main functions two(view flow diagram).

5. *Frontend Microservice*: This small Microservice have two interfaces
    - Upload photos and her diseases
    - Browser similar photos of a leaf plant image, with its possible disease.

## Sequence diagram
![SequenceDiagram](designs/sequence\ diagram.png)







