from flask import Flask
from flask_pymongo import PyMongo
import os
app = Flask(__name__)
# --------------- environments  ------------------------
app.config['MONGO_URI'] = os.environ["MONGO_URI"]  if "MONGO_URI" in os.environ else  'mongodb://localhost:27017/pythonmongodb'
app.config["MATCHER_URL"] = os.environ["MATCHER_URL"] if "MATCHER_URL" in os.environ else "http://localhost:8002/"
app.config['UPLOAD_FOLDER'] = os.environ["UPLOAD_FOLDER"] if "UPLOAD_FOLDER" in os.environ  else 'static/uploads/'
app.secret_key = os.environ["SECRET_KEY"] if "SECRET_KEY" in os.environ else 'myawesomesecretkey'


# ----- connect to database -----------
mongo = PyMongo(app)


def get_env(key, default=None):
    if key in app.config:
        return app.config[key]
    return default