from flask import Flask, jsonify, request,Response
from requests_toolbelt import MultipartEncoder
from settings import app
import controllers
from json import loads 
# ------ routes -------------------
@app.route("/",methods=["GET"])
def hello():
    return "works"
#https://roytuts.com/upload-and-display-multiple-images-using-python-and-flask/
@app.route('/upload_image', methods=['POST'])
def upload_images():
    # receiving images
    uploaded_files = list(request.files.listvalues())
    
    if not uploaded_files:
        return make_error_response("No file part",500)
    # receiving features data
    if "all_images_features" not in request.form or 'disease' not in request.form:
        return make_error_response("No  data part",500)
    
    all_images_features = request.form.getlist('all_images_features')
    
    all_images_features = tuple(map(eval,all_images_features))

    disease = request.form.get('disease')
    # validate same length data and files
    if len(all_images_features) != len(uploaded_files):
        return make_error_response("wrong post",500)
    try:
        controllers.upload_images(uploaded_files,all_images_features,disease)
    except Exception as e:
        return make_error_response(getattr(e, 'message', repr(e)),500)


    return "ok"

@app.route('/filter', methods=['GET'])
def filter_images():  
    
    if "features" not in request.form:
        return make_error_response("No features data part",500)
    features = request.form.getlist('features')
    features = tuple(map(float,features))
    images =  controllers.filter_images(features)
    m = MultipartEncoder(images)
    return Response(m.to_string(), mimetype=m.content_type)
    
#curl -X GET "http://localhost:5000/filter" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{\"features\":[0,1,2,3,8]}"


# ------------ error handlers ---------------------

@app.errorhandler(404)
def not_found(error=None):
    message = {
        'message': 'Resource Not Found ' + request.url,
        'status': 404
    }
    response = jsonify(message)
    response.status_code = 404
    return response

@app.errorhandler(500)
def make_error_response(msg,error_code):
    response = jsonify({
            'error': msg
        })
    response.status_code= error_code
    return response


if __name__ == "__main__":
    app.run(debug=True,host ='0.0.0.0')


# https://www.youtube.com/watch?v=GsCCyN3fRoI 
#https://github.com/JexPY/filemanager-fastapi/blob/master/api/app/services/serveUploadedFiles.py


