import os
from typing import List
from fastapi import FastAPI,Query
from fastapi.middleware.cors import CORSMiddleware

from .comparer import Comparer

app = FastAPI()
origins = [
    "http://storage:5000",
    'http://localhost:5000',
    '*'
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
)
#https://fastapi.tiangolo.com/tutorial/request-files/
@app.get("/")
def hola():
    
    return {"hola":"mensaje de retorno"}

@app.get("/match")
async def match(methods: List[str] = Query(...), features_1 :List[float] = Query(...),features_2: List[float] =Query(...) ):
    comparer = Comparer()
    match = comparer.match(features_1,features_2, 1e-10)
    print("match in: ",match)
    return match